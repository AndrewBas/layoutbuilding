import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:layoutbuilding_app/second_screen.dart';
import 'package:layoutbuilding_app/test_container.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,

        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);



  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {

      _counter++;
    });
  }


  void gotoSecondScreen(BuildContext ctx){
    _incrementCounter();
    Navigator.of(ctx).push(MaterialPageRoute(builder: (_) {
      return SecondScreen();
    }
    ),
    );
  }
 /* void _gotoSecondScreen() {
    setState(() {

    });

  }*/
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: Center(

        child: Column(

          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            LayoutBuilder(
              builder: (context, constraints) {
               return AutoSizeText('some text',
               style: TextStyle(fontSize: 20),
                maxLines: 2,
               );
              },
            ),
            LayoutBuilder(
              builder: (context, constraints) {
                if (_counter < 3) {
                  return FlatButton(onPressed: () => {}, child: TestContainer(
                    width: 250.0,
                    height: 150.0,
                    text: 'BIG',),
                  );
                }
                else if (_counter > 3 && _counter < 5) {
                  return FlatButton(
                    child: TestContainer(
                      width: 50.0,
                      height: 150.0,
                      text: 'BIG',
                    ),
                      onPressed: () => {},
                  );
                }
                return TestContainer(text: 'SMALL');
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => gotoSecondScreen(context),
        tooltip: 'Increment',
        child: Icon(Icons.logout),
      ),
    );
  }
}
