import 'package:flutter/material.dart';

class SecondScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Other text'),
      ),
      body: Container(
        child: Text('The Second Screen'),
      ),
    );
  }
}
